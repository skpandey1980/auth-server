#!/bin/bash

# To get a JWT token for the admin, first run this: ./curlAdmin.sh in the terminal.
# The system will return a error message to the terminal; the message contains a "mfa-token" line.
# Copy the value of the line (without "") and paste it to the command: ./curlTFAMe.sh abcd-----------yz .
# The system will return a JWT token of the admin with ROLE_ADMIN and ROLE_USER authorities.
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto

json=$(curl trusted-app:secret@localhost:9999/oauth/token -d grant_type=password -d username=admin -d password=password ) &&
token=$(echo $json | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g") &&

json=$(curl trusted-app:secret@localhost:9999/oauth/token -d grant_type=tfa -d tfa_token=$token -d tfa_code=197214 ) &&

token=$(echo $json | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")

echo $(jwt-decode.payload $token)
