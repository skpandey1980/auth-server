#!/bin/bash

export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto


json=$( curl trusted-app:secret@localhost:9999/oauth/token -d grant_type=password -d username=user -d password=password )
token=$(echo $json | sed "s/{.*\"refresh_token\":\"\([^\"]*\).*}/\1/g")

json=$(curl trusted-app:secret@localhost:9999/oauth/token -d grant_type=refresh_token -d refresh_token=$token )
#echo $json
token=$(echo $json | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")

echo $(jwt-decode.payload $token)

