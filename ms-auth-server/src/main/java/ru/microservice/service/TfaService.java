package ru.microservice.service;

public interface TfaService {

    boolean isEnabled(String username);

    boolean verifyCode(String username, int code);
}
