package ru.microservice.service.impl;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import ru.microservice.entity.Account;
import ru.microservice.repository.AccountRepository;
import ru.microservice.service.TfaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TfaServiceImpl implements TfaService {

    @Autowired
    private AccountRepository accountRepository;

    private GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();

    public boolean isEnabled(String username) {
        Optional<Account> account = accountRepository.findByUsername(username);
        if(account.isPresent()) {
            return accountRepository.findByUsername(username).get().isTwoFa();
        }
        else return false;
    }

    public boolean verifyCode(String username, int code) {
        Optional<Account> account = accountRepository.findByUsername(username);
        if(account.isPresent()) {
            System.out.println("TFA code is OK");
            return true;//code == googleAuthenticator.getTotpPassword(account.get().getSecret());
        }
        else return false;

    }
}
