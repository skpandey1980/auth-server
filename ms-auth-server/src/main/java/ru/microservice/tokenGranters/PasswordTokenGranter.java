package ru.microservice.tokenGranters;

import ru.microservice.service.TfaService;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class PasswordTokenGranter extends AbstractTokenGranter {
    private static final String GRANT_TYPE = "password";
    private static final GrantedAuthority PRE_AUTH = new SimpleGrantedAuthority("PRE_AUTH");

    private final ClientDetailsService clientDetailsService;


    private final AuthenticationManager authenticationManager;
    private final TfaService tfaService;
    private final DefaultTokenServices jwtService;

    private final AuthorizationServerEndpointsConfigurer endpointsConfigurer;

    public PasswordTokenGranter(AuthorizationServerEndpointsConfigurer endpointsConfigurer, AuthenticationManager authenticationManager, TfaService tfaService
    ,DefaultTokenServices jwtService

    ) {

        super(jwtService, endpointsConfigurer.getClientDetailsService(), endpointsConfigurer.getOAuth2RequestFactory(), GRANT_TYPE);
        this.authenticationManager = authenticationManager;
        this.tfaService = tfaService;
        this.endpointsConfigurer = endpointsConfigurer;
        this.clientDetailsService = endpointsConfigurer.getClientDetailsService();
        this.jwtService = jwtService;

    }

    public OAuth2AccessToken grant(String grantType, TokenRequest tokenRequest) {
        if (!grantType.equals(GRANT_TYPE)) {
            return null;
        } else {
            Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
            String username = parameters.get("username");
            String password = parameters.get("password");
            parameters.remove("password");
            Authentication userAuth = new UsernamePasswordAuthenticationToken(username, password);
            ((AbstractAuthenticationToken) userAuth).setDetails(parameters);

            String clientId = tokenRequest.getClientId();
            ClientDetails client = this.clientDetailsService.loadClientByClientId(clientId);

            this.validateGrantType(grantType, client);

            try {
                userAuth = this.authenticationManager.authenticate(userAuth);
            } catch (AccountStatusException | BadCredentialsException e) {
                throw new InvalidGrantException(e.getMessage());
            }

            if (userAuth != null && userAuth.isAuthenticated()) {
                OAuth2Request storedOAuth2Request = this.getRequestFactory().createOAuth2Request(client, tokenRequest);
                if (tfaService.isEnabled(username)) {
                    userAuth = new UsernamePasswordAuthenticationToken(username, password, Collections.singleton(PRE_AUTH));

                    OAuth2AccessToken accessToken = this.endpointsConfigurer.getTokenServices().createAccessToken(new OAuth2Authentication(storedOAuth2Request, userAuth));
                    return accessToken;
                }

                OAuth2AccessToken jwtToken = this.jwtService.createAccessToken(new OAuth2Authentication(storedOAuth2Request, userAuth));
                return jwtToken;
            } else {
                throw new InvalidGrantException("Could not authenticate user: " + username);
            }
        }
    }

}