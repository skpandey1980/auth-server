package ru.microservice.repository;

import ru.microservice.entity.Mail;
import org.springframework.data.repository.CrudRepository;


public interface MailRepository extends CrudRepository<Mail, Long> {

}