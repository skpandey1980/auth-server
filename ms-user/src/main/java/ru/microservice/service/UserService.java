package ru.microservice.service;

import ru.microservice.entity.User;

public interface UserService {

    User registerUser(User input);

    Iterable<User> findAll();

    User findById(Long id);
}
